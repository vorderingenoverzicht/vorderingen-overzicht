# Releasing

[[_TOC_]]

## How to release a component (except user_ui)
- Update version number in `helm/Chart.yaml`:

```yaml
...
version: X.Y.Z
appVersion: X.Y.Z
...
```

- Create MR and merge to `master`
- Tag `master` with `vX.Y.Z`
- Wait for component pipeline to complete
- Wait for Chart repository pipeline to complete
- Update version of components in [vorijk-demo-environment repository](https://gitlab.com/blauwe-knop/vorderingenoverzicht/vorijk-demo-environment)
- Create MR and merge to `master`.

As we use [Flux](https://fluxcd.io), as GitOps deployment, the new components will be deployed after merging.

In some cases a container image is not ready when Flux tries to deploy the new components. In that case we need to reconcile the source per namespaces:
```sh
flux reconcile source helm vorijk -n tn-vorijk-bk-app
flux reconcile source helm vorijk -n tn-vorijk-bk-belastingdienst
flux reconcile source helm vorijk -n tn-vorijk-bk-cak
flux reconcile source helm vorijk -n tn-vorijk-bk-cjib
flux reconcile source helm vorijk -n tn-vorijk-bk-duo
flux reconcile source helm vorijk -n tn-vorijk-bk-gemeente-tilburg
flux reconcile source helm vorijk -n tn-vorijk-bk-logius
flux reconcile source helm vorijk -n tn-vorijk-bk-nio
flux reconcile source helm vorijk -n tn-vorijk-bk-rci
flux reconcile source helm vorijk -n tn-vorijk-bk-rmb
flux reconcile source helm vorijk -n tn-vorijk-bk-rvo
flux reconcile source helm vorijk -n tn-vorijk-bk-scheme
flux reconcile source helm vorijk -n tn-vorijk-bk-svb
flux reconcile source helm vorijk -n tn-vorijk-bk-uwv

flux reconcile source helm vorijk -n tn-vorijk-bk-tools
```

For the test namespaces:
```sh
flux reconcile source helm vorijk -n tn-vorijk-bk-test-app
flux reconcile source helm vorijk -n tn-vorijk-bk-test-belastingdienst
flux reconcile source helm vorijk -n tn-vorijk-bk-test-cak
flux reconcile source helm vorijk -n tn-vorijk-bk-test-cjib
flux reconcile source helm vorijk -n tn-vorijk-bk-test-duo
flux reconcile source helm vorijk -n tn-vorijk-bk-test-gemeente-tilburg
flux reconcile source helm vorijk -n tn-vorijk-bk-test-logius
flux reconcile source helm vorijk -n tn-vorijk-bk-test-nio
flux reconcile source helm vorijk -n tn-vorijk-bk-test-rci
flux reconcile source helm vorijk -n tn-vorijk-bk-test-rmb
flux reconcile source helm vorijk -n tn-vorijk-bk-test-rvo
flux reconcile source helm vorijk -n tn-vorijk-bk-test-scheme
flux reconcile source helm vorijk -n tn-vorijk-bk-test-svb
flux reconcile source helm vorijk -n tn-vorijk-bk-test-uwv

flux reconcile source helm vorijk -n tn-vorijk-bk-test-tools
```
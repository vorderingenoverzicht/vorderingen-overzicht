# Development Environment Mac

General developer instructions are in [DEVELOPER.md](DEVELOPER.md).
This file lists the OS specific instructions for Mac OS.

[[_TOC_]]


## Run
### Minikube (Mac Intel amd64)
[Ingress DNS](https://minikube.sigs.k8s.io/docs/handbook/addons/ingress-dns/).

```sh
skaffold config set --global local-cluster true

minikube start --driver=hyperkit

minikube addons enable ingress
minikube addons enable ingress-dns

sudo mkdir /etc/resolver
sudo touch /etc/resolver/bk

echo "domain bk
search bk
nameserver $(minikube ip)
search_order 1
timeout 5" | sudo tee -a /etc/resolver/bk
# restart dns
sudo killall -HUP mDNSResponder
# verify that your dns bk resolver shows here
scutil --dns
```

### Minikube (Mac Apple Silicon arm)
```sh
skaffold config set --global local-cluster true

minikube start

minikube addons enable ingress
minikube addons enable ingress-dns
```

Change coredns to resolve ingress urls within cluster.

```sh
kubectl edit configmap coredns -n kube-system
```

Add the following part and replace `192.168.49.2` with your machine's IP, which can be found by running `minikube ip`:

```yaml
...
data:
  Corefile: |
    .:53 {
        ...
    }
    bk:53 {
        errors
        cache 30
        forward . 192.168.49.2
    }
kind: ConfigMap
...
```

```sh
sh scripts/update-hosts.sh
```

To allow ingress, run `minikube tunnel` in a separate terminal and keep it running.
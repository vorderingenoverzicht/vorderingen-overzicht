# Development Environment Linux

General developer instructions are in [DEVELOPER.md](DEVELOPER.md).
This file lists the OS specific instructions for Linux.
This guide assumes the use of Ubuntu linux.
Another distro probably works too, but this isn't verified.

[[_TOC_]]

### Linux prerequisites
Prerequisites required by minikube ingress driver:
```
sudo apt-get install qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils
```

## Run
### Minikube (Linux)
Recommended driver for Linux is [Docker](https://minikube.sigs.k8s.io/docs/drivers/docker/), KVM2 is hard to set up with new kernels due to CGroupsv1 being deprecated.

Start and config Minikube:
```shell
skaffold config set --global local-cluster true

minikube start

minikube addons enable ingress
minikube addons enable ingress-dns
```

To enable ingress there are 2 options:
- recommended: update the host file using the [update-hosts.sh](scripts/update-hosts.sh) script (make sure Minikube is running 
and applications are installed for this, because it copies the hostnames from the running application ingresses)
- alternative: install dnsmasq
  - in dnsmasq config add server=/bk/192.168.39.33 and replace the ip address with the minikube ip (note every minikube delete/start results in a new minikube ip)

Change coredns to resolve ingress urls within cluster:
```sh
kubectl edit configmap coredns -n kube-system
```

Add the following part and replace `192.168.49.2` with your machine's IP, which can be found by running `minikube ip`:
```yaml
...
data:
  Corefile: |
    .:53 {
        ...
    }
    bk:53 {
        errors
        cache 30
        forward . 192.168.49.2
    }
kind: ConfigMap
...
```

Add route and IP to the sudoers list with NOPASSWD, by running `sudo visudo`:
```sh
username ALL=(ALL) NOPASSWD: /usr/sbin/route
username ALL=(ALL) NOPASSWD: /usr/sbin/ip
```

For the android simulator: set dns ip to 10.0.2.2 (redirect to localhost on dev machine)

## Problems & Solutions
### Docker BuildKit missing
Problem:  
```
ERROR: BuildKit is enabled but the buildx component is missing or broken.
       Install the buildx component to build images with BuildKit:
       https://docs.docker.com/go/buildx/
```
Solution:  
Probably `docker.io` only is installed, so `docker-buildx-plugin` is missing.  
This can be solved by installing `docker-ce`:
```
sudo apt install docker-ce
```

### Too many open files
Problem:  
```
failed to create fsnotify watcher: too many open files
```
Solution:  
The OS is configured to have limited amount of files open, which can be too much if running the minikube environment with all backends.  
This can be solved by increasing the amount of open files allowed. Run `scripts/calculate-sensible-open-files.sh` to find out what a sensible setting should be for your specific system and run the suggested commands manually with the given values.
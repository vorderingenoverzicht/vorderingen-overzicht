#!/bin/bash

# RUN THIS COMMAND FROM THE ROOT FOLDER OF A FLUTTER PACKAGE

# Get the directory of this script
SCRIPT_DIR=$(dirname "$BASH_SOURCE")

# Change to the directory from which the script was called
cd "$(pwd)"

# Run dart format using fvm
fvm dart format .

# Run flutter analyze using fvm
fvm flutter analyze

# Prompt user for commit or amend
read -p "Do you want to commit (c) or amend (a) the changes? " option

if [ "$option" = "c" ]; then
  # Prompt user for a commit message
  read -p "Enter your commit message: " commit_message

  # Commit changes
  git add .
  git commit -m "$commit_message"
  echo "New commit created."

elif [ "$option" = "a" ]; then
  # Amend the previous commit
  git add -A
  git commit --amend
  echo "Changes amended to the previous commit."

else
  echo "Invalid option. No commit or amend performed."
fi

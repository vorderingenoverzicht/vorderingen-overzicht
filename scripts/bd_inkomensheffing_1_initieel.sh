
curl "http://demo-simulation-process.bk-manager.source-organization.vorderingenoverzicht.blauweknop.bk/v4/add_contact_opties" \
  -X POST \
  -d @json/bd_inkomensheffing_1_initieel-1-contact.json \
  -H "content-type: application/json"

curl "http://demo-simulation-process.bk-manager.source-organization.vorderingenoverzicht.blauweknop.bk/v4/add_event" \
  -X POST \
  -d @json/bd_inkomensheffing_1_initieel-2.json \
  -H "content-type: application/json"


curl "http://demo-simulation-process.bk-manager.source-organization.vorderingenoverzicht.blauweknop.bk/v4/add_event" \
  -X POST \
  -d @json/bd_inkomensheffing_1_initieel-3.json \
  -H "content-type: application/json"

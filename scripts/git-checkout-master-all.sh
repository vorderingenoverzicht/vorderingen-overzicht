#!/bin/bash

scriptPath=${0%/*}
. "$scriptPath/projects.sh"

pwd
cd ../..

for dir in $directories_vorderingenoverzicht; do
  cd "vorderingenoverzicht/$dir" || exit
  pwd
  git stash
  git checkout master
  git pull
  git stash pop
  cd ../..
done

for dir in $directories_common; do
  cd "common/$dir" || exit
  pwd
  git stash
  git checkout master
  git pull
  git stash pop
  cd ../..
done

for dir in $directories_connect; do
  cd "connect/$dir" || exit
  pwd
  git stash
  git checkout master
  git pull
  git stash pop
  cd ../..
done

for dir in $directories_tools; do
  cd "tools/$dir" || exit
  pwd
  git stash
  git checkout master
  git pull
  git stash pop
  cd ../..
done

cd vorderingenoverzicht/development-environment || exit
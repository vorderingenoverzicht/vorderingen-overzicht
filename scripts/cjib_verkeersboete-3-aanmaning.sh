curl "http://demo-simulation-process.bk-manager.source-organization.vorderingenoverzicht.blauweknop.bk/v4/add_event" \
  -X POST \
  -d @json/cjib_verkeersboete_3_aanmaning-1.json \
  -H "content-type: application/json"

curl "http://demo-simulation-process.bk-manager.source-organization.vorderingenoverzicht.blauweknop.bk/v4/add_event" \
  -X POST \
  -d @json/cjib_verkeersboete_3_aanmaning-2.json \
  -H "content-type: application/json"

curl "http://demo-simulation-process.bk-manager.source-organization.vorderingenoverzicht.blauweknop.bk/v4/add_event" \
  -X POST \
  -d @json/cjib_verkeersboete_3_aanmaning-3.json \
  -H "content-type: application/json"

curl "http://demo-simulation-process.bk-manager.source-organization.vorderingenoverzicht.blauweknop.bk/v4/add_achterstand" \
  -X POST \
  -d @json/cjib_verkeersboete_3_aanmaning-4-achterstand.json \
  -H "content-type: application/json"

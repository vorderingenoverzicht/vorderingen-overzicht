#!/bin/bash



directories_vorderingenoverzicht='charts
user_ui
service-discovery-process
session-process
session-service
scheme-db
scheme-management-ui
scheme-process
scheme-management-process
scheme-service
bk-management-process
bk-management-ui
bk-config-service
bk-config-db
citizen-financial-claim-process
docs
mock-source-system
demo-source-system
demo-simulation-process
financial-claim-request-service
financial-claim-request-db
fcid_view_validator
fcid_library
app-management-service
mock-digid-ui
app-management-process
app-management-ui
app-management-db
source-organization-implementation-test
monitoring'

directories_common='hardware-keystore
health-checker'

directories_connect='dart-connect
go-connect'

directories_tools='scenario_manager'

directories="${directories_vorderingenoverzicht}
${directories_common}
${directories_connect}
${directories_tools}"
# Development Environment

[[_TOC_]]

## Installation

Prerequisites:

- [Git](https://git-scm.com/) (latest version)
- [Docker](https://docs.docker.com/engine/install/) (latest version)
- [Golang](https://go.dev/doc/install) (latest version)
- [GoMock](https://github.com/uber-go/mock) (latest version) (note: this should NOT be the deprecated golang gomock!)
- [Flutter](https://flutter.dev/docs/get-started/install) (latest version) (note: on Ubuntu 2023.10 Flutter is broken, to fix: [here](https://community.localwp.com/t/i-can-not-install-localwp-into-ubuntu-23-10-because-comes-back-with-error-dependency-is-not-satisfiable-libncurses5/38920/5))
- [kubectl](https://kubernetes.io/docs/tasks/tools/) (latest version, should be compatible with Minikube/cluster version)
- [Skaffold](https://skaffold.dev/docs/install/)
- [Minikube](https://minikube.sigs.k8s.io/docs/start/) (latest version, on Linux install [Linux prerequisites](DEVELOPER_LINUX#linux-prerequisites) first)
- [Helm](https://helm.sh/docs/intro/install/) (latest version)
- [Android sdk tool](https://developer.android.com/tools) (latest version, also comes with Android Studio)

Recommended:

- [k9s](https://k9scli.io/) (latest version)
- [FVM](https://fvm.app/documentation/getting-started/installation) (latest version)

## Get started

Create a local folder where you will store the repositories, for example, `/home/[user-name]/Vo-Rijk`.
Next we have three "groups" to clone. Make sure to create the following folder in the "Vo-Rijk" folder:

```sh
mkdir -p vorderingenoverzicht \
&& mkdir -p common \
&& mkdir -p connect \
```

Clone the development-environment repo into this folder.

Execute the [git-clone-all.sh](scripts/git-clone-all.sh) script located in the `scripts` folder to clone all the missing repositories to your environment:

```sh
sh scripts/git-clone-all.sh
```

### Visual Studio Code

Install [Visual Studio Code](https://code.visualstudio.com/Download).
Open the [workspace file](vorderingenoverzicht.code-workspace) with Visual Studio Code.
You will then see an overview in VS Code with all workspaces, all of which are empty except for the development-environment folder.

### Goland / Android Studio

Install [Jetbrains Toolbox](https://www.jetbrains.com/toolbox-app/) and use that to install Goland for backend development and Android Studio for frontend development.
Or use IntelliJ Ultimate to combine the both using plugins.
Install the following plugins:
```
Flutter
Dart
```
Note: you might still need to install Flutter (and Dart). E.g. on Ubuntu using snap:
```
sudo snap install flutter --classic
```

## Run

### Minikube

Configuration of Minikube is dependent on the OS being used. Follow the configuration of your environment below.
Linux config: [Minukube Linux](DEVELOPER_LINUX.md#minikube-linux)
Mac Intel AMD64 config: [Minukube Mac Intel AMD64](DEVELOPER_MAC.md#minikube-mac-intel-amd64)
Mac Apple Silicon ARM config: [Minukube Mac Apple Sillicon ARM](DEVELOPER_MAC.md#minikube-mac-apple-silicon-arm)

### Backend run

**Set context and create namespaces:**
Run the script [generate-minikube-context-and-namespaces.sh](scripts/generate-minikube-context-and-namespaces.sh) located in the `scripts` folder:

```sh
sh scripts/generate-minikube-context-and-namespaces.sh
```

**Run CloudNativePG operator:**

```sh
skaffold run -f=skaffold.cloud-native-pg-operator.yaml
```

<br/>
<br/>
Run the following two `skaffold dev` commands in separate terminals, as they will remain active until manually stopped.

**Run and watch files for scheme and source organization:**

```sh
skaffold dev -f=skaffold.yaml
```

**Run and watch files for app manager:**

```sh
skaffold dev -f=skaffold.app-manager.yaml
```

> 💡 Tip:
>
> When executing these scripts, be aware that minikube tunnel may request your password initially. You will need to input your password in the terminal where minikube tunnel is active.

## Run the app (user_ui)

Cd into the project folder of the `user_ui` project. Development on the app can be done using Visual Studio Code, or by using Android Studio or by using Intellij. Please choose your personal favorite.

### Visual Studio Code

To manage Flutter installations with ease, it possible to use Flutter Version Manager (FVM). You can choose to install it globally or per project.
Check the required flutter version in `user_ui/.fvm/fvm_config.json` and run the following command to use that specific version:

```shell
fvm use {version}
```

Run `flutter pub get` to update all the dependencies.
In the vscode launch settings are 3 configurations listed. Select the most relevant one and run the app:

- new mock - runs the app with mock instances
- new local - runs the app against the local setup kubernetes cluster
- new production - runs the app against the production database

### Android Studio

Run `flutter pub get` to update all the dependencies.
Running the tests requires a local installation of sqlite3. On Ubuntu perform the following:
```
sudo apt-get install sqlite3 libsqlite3-dev
sudo apt-cache policy sqlite3
```
Don't forget to enable Dart support for the project under 'Settings->Languages&Frameworks->Dart'. 
When Flutter is installed using Ubuntu snap, then the Dart SDK is:
```
~/snap/flutter/common/flutter/bin/cache/dart-sdk
```

### Commandline

To run the app, there are three options:

- against production Kubernetes cluster: `flutter run`
- against local Kubernetes cluster: `flutter run -t lib/main_local.dart`
- against mock instances: `flutter run -t lib/main_mock.dart`

To run the app as a web version in Google Chrome, add `-d chrome` to the command.
To use Chromium in this situation, add the following environment variable:

```sh
export CHROME_EXECUTABLE=chromium
```

## Test

### Backend test

Regenerate all gomock files:

```sh
cd scripts && ./regenerate-all-gmock-files.sh
```

To run tests, go to the root folder of the component to test and run:

```sh
go test ./... -v
```

## Cleanup kubernetes

```sh
kubectl delete namespace bk-cloud-native-pg-operator
kubectl delete namespace bk-citizen
kubectl delete namespace bk-scheme
kubectl delete namespace bk-source-organization-manager
kubectl delete namespace bk-app-manager-organization
kubectl delete namespace bk-digid-organization
```

## K9S

K9s is a convenient command-line tool that allows you to manage your clusters with ease and style.
To launch K9s in your CLI, simply type `k9s`.
Once K9s is up and running, you'll start in the default namespace. You can view all available namespaces in the cluster by pressing `0`.
In the namespace list, the `Status` column indicates whether a specific namespace is running successfully or has completed its tasks.
Make sure to verify that all namespaces are either in the `completed` or `running` status.

## Register a source organization

When you first start the app, there won't be a source organization available by default. You need to add it manually.
To do this, follow these steps:

1. Run the following command to retrieve a list of all available endpoints in the cluster:

```shell
kubectl get ing -A
```

2. Copy the URL from the HOST column for both `bk-management-ui` and `scheme-management-ui`. Open them in your browser and remember to include `http://` (not `https`!) at the beginning of the URL.

### Source organisation management UI

This UI allows you to create and register a source organization, and it consists of three steps:

1. In this window, generate a Keypair by clicking the Generate (`Genereren`) button.
2. Next, register the organization by clicking the Register (`Aanmelden`) button.
3. Once the organization is approved by the scheme manager, click the Verify (`Controleren`) button.

### Scheme management UI

The Scheme Management UI is used by the scheme manager to oversee all participating organizations.
When an organization submits a registration request, you will need to approve it by clicking the Approve (`Goedkeuren`) button.
After registering and approving an organization, it should appear in the user_ui app.


## Problems & Solutions

### Build failing because of not enough disk space
Problem:
```
 no space left on device
```
Solution:
Prune docker using `minikube ssh -- docker system prune` and if this problem reoccurs increase the disk size using `minikube config set disk-size 40GB`.

### Pods not starting due to starvation
Problem:
Pods don't start due to no available cpu cores
Solution:
increase number of cpu cores available to the cluster using `minikube config set cpus 4`.